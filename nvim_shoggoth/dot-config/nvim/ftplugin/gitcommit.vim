setlocal tw=79
setlocal spell
let b:switch_definitions =
	\ [
	\	[ 'major', 'minor', 'patch' ]
	\ ]

let s:get_jira_ref_cmd = "git symbolic-ref --short HEAD | sed -r 's/^.*([a-zA-Z]{2}-[0-9]+).*$/\\1/'"
normal ggO+semver: minor+ref: =system(s:get_jira_ref_cmd)
0 " put cursor back to the top
