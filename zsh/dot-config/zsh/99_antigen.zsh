export ANTIGEN_CACHE=false


antigen_repo=https://github.com/zsh-users/antigen.git

antigen_dir=${HOME}/.antigen-repo

[[ -d ${antigen_dir} ]] || git clone --depth=1 ${antigen_repo} ${antigen_dir}

source ${antigen_dir}/antigen.zsh

# antigen doesn't like being run without compoletion being inited
compinit

antigen use oh-my-zsh
antigen bundles <<EOF
  aws
  colorize
  common-aliases
  djui/alias-tips
  fancy-ctrl-z
  git
  git-auto-fetch
  sudo
  systemd
  vi-mode
  zsh-users/zsh-autosuggestions
  zsh-users/zsh-syntax-highlighting
  zsh_reload

EOF

ZSH_THEMES=($(cat ~/.config/zsh/zsh_themes ))
ZSH_THEME=${ZSH_THEMES[RANDOM % $#ZSH_THEMES + 1]}

antigen theme $ZSH_THEME
antigen apply
