filetype plugin indent on

setlocal expandtab
setlocal tabstop=4
setlocal shiftwidth=4
setlocal softtabstop=4

let b:dispatch = '%'

let b:ale_linters = ['pycodestyle', 'pylint', 'pydocstyle']
let b:ale_fixers = ['black', 'isort']

let g:ale_python_pylint_change_directory = 0


nnoremap <buffer> <silent> <Leader>nt :call neoterm#do('pipenv run nose2')<cr>
