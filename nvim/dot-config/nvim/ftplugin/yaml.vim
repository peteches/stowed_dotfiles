setlocal sw=8
setlocal ts=8
setlocal sts=8
setlocal noexpandtab
let b:ale_fixers = ['prettier']
