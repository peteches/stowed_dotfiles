let b:ale_terraform_fmt_executable = 'tf'
let b:ale_terraform_terraform_executable = 'tf'

setlocal expandtab
setlocal tabstop=4
setlocal shiftwidth=4
setlocal softtabstop=4
