"Setup Plugins: {{{
call plug#begin('~/.config/nvim/plugged')
" Install plugins {{{
Plug 'AndrewRadev/switch.vim'
Plug 'Glench/Vim-Jinja2-Syntax'
Plug 'Shougo/deoplete.nvim'
Plug 'Shougo/neco-vim'
Plug 'airblade/vim-gitgutter'
Plug 'chrisbra/csv.vim'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'dougireton/vim-chef'
Plug 'equalsraf/neovim-gui-shim'
Plug 'fishbullet/deoplete-ruby'
Plug 'hashivim/vim-terraform'
Plug 'johngrib/vim-game-code-break'
Plug 'liuchengxu/vim-which-key'
Plug 'mileszs/ack.vim'
Plug 'mrk21/yaml-vim'
Plug 'obreitwi/vim-sort-folds'
Plug 'pbrisbin/vim-mkdir'
Plug 'pearofducks/ansible-vim'
Plug 'rogerz/vim-json'
Plug 'ryanoasis/vim-devicons'
Plug 'tommcdo/vim-exchange'
Plug 'tommcdo/vim-fubitive'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-capslock'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-dispatch'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-obsession'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-sleuth'
Plug 'tpope/vim-speeddating'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'vasconcelloslf/vim-interestingwords'
Plug 'vim-airline/vim-airline'
Plug 'vimwiki/vimwiki'
Plug 'voldikss/vim-floaterm'
Plug 'w0rp/ale'
Plug 'zchee/deoplete-jedi'
" }}}
" Treat colors from repos as plugins {{{
Plug 'dylanaraps/wal.vim'
" }}}
call plug#end()
" }}}

" Functions {{{

	function! TrimWhiteSpace() " {{{
		%s/\s\+$//e
	endfunction
	" }}}

	function! IsBufferBlank() " {{{
		if getline(1, '$') == [''] && ! &mod
			return 1
		else
			return 0
		endif
	endfunction
	" }}}

	function! OpenRC() " {{{
		if IsBufferBlank()
			:edit $MYVIMRC
		else
			:vnew $MYVIMRC
		endif
	endfunction
	" }}}

	function! OpenFTplugin() " {{{
		let s:path = stdpath('config') . '/ftplugin/' . &filetype . '.vim'
		if IsBufferBlank()
			:execute 'edit ' . s:path
		else
			:execute 'vnew ' . s:path
		endif
	endfunction
	" }}}

	function! RunMakeTarget() " {{{

		if exists('g:petechesMakeTarget')
			:execute 'FloatermNew make ' . g:petechesMakeTarget . '<Enter>'
		elseif exists('b:petechesMakeTarget')
			:execute 'FloatermNew make ' . b:petechesMakeTarget . '<Enter>'
		else
			:execute 'FloatermNew make all'
		endif
	endfunction
	"}}}

" }}}

"Plugin Settings {{{
	"Deoplete {{{
	let g:deoplete#enable_at_startup = 1
	" }}}

	"Ack {{{
	let g:ack_use_dispatch = 1
	" }}}

	"Airline {{{
	let g:airline#extensions#tabline#enabled = 1
	let g:airline#extensions#ale#enabled = 1
	let g:airline_powerline_fonts = 1
	" }}}

	" CtrlP {{{
	let g:ctrlp_show_hidden = 1
	" }}}

	" ALE {{{
	let g:ale_fix_on_save = 1
	let g:ale_echo_msg_format = '[%linter%] [%severity%] %s'


	" }}}

	" Switch {{{
	let g:switch_mapping = "-"
	let g:switch_custom_definitions =
	\ [
		\	['yes', 'no'],
		\	['true', 'false'],
		\	['left', 'right'],
		\	['on', 'off'],
		\	['||', '&&'],
		\	['udp', 'tcp'],
		\	['low', 'medium', 'high'],
		\ ]
	" }}}

	" neoterm {{{
	let g:neoterm_default_mod = 'vertical'
	let g:neoterm_automap_keys = '<Leader>tt'
	let g:neoterm_autoscroll = 1
	" }}}

	" floaterm {{{
	let g:floaterm_winblend = 9
	let g:floaterm_position = 'random'
	let g:floaterm_autoclose = 1
	let g:floaterm_rootmarkers = [ '.git', '.gitignore' ]
	let g:floaterm_keymap_prev = '<leader>tp'
	let g:floaterm_keymap_next = '<leader>tn'
	let g:floaterm_keymap_first = '<leader>tP'
	let g:floaterm_keymap_last = '<leader>tN'
	let g:floaterm_keymap_hide = '<leader>th'
	let g:floaterm_keymap_show = '<leader>ts'
	let g:floaterm_keymap_kill = '<leader>tk'
	let g:floaterm_keymap_toggle = '<leader>tT'
	nnoremap <leader>tR :FloatermNew ranger
	nnoremap <leader>tt :call RunMakeTarget()<Enter>
	" }}}

	" which-keys {{{
	let g:which_key_map = {}
	let g:which_key_use_floating_win = 1
	nnoremap <leader> :<c-u>WhichKey '\'<Enter>
	nnoremap ] :<c-u>WhichKey ']'<Enter>
	nnoremap [ :<c-u>WhichKey '['<Enter>

	if has('autocmd')
		augroup WHICHKEYS
			autocmd!
			autocmd FileType which_key highlight WhichKey ctermbg=None ctermfg=7
			autocmd FileType which_key highlight WhichKeySeperator ctermbg=None ctermfg=7
			autocmd FileType which_key highlight WhichKeyGroup cterm=bold ctermbg=None ctermfg=7
			autocmd FileType which_key highlight WhichKeyDesc ctermbg=None ctermfg=7
			autocmd FileType which_key highlight WhichKeyFloating ctermbg=None ctermfg=7
		augroup END

	endif
	" }}}

" }}}

"General settings {{{
set inccommand=split
set list
set listchars=eol:§,tab:▸›,extends:»,precedes:«,nbsp:•,trail:•
set mouse=a
set nohlsearch
set noshowmode
set number
set relativenumber
set shada=%0,'10,<1000,h,n~/.config/nvim/shada
set showcmd
if filewritable("$HOME/.config/nvim/undos/") != 2
	call system("mkdir -p $HOME/.config/nvim/undos")
endif
set undodir=$HOME/.config/nvim/undos/
set undofile
set wildignore=build,*.pyc,venv
set wildmode=list:full
" }}}

"Backup settings {{{
if filewritable("$HOME/.config/vim/backups/") != 2
	call system("mkdir -p $HOME/.config/nvim/backups")
endif
set backup
set backupdir=$HOME/.config/nvim/backups
set backupcopy=auto
set writebackup
" }}}

"Key Mappings {{{
	"Window movements {{{
	inoremap <C-H> <Esc><C-W>H
	noremap <C-H> <Esc><C-W>H
	tnoremap <C-H> <Esc><C-W>H
	inoremap <C-h> <Esc><C-W>h
	noremap <C-h> <Esc><C-W>h
	tnoremap <C-h> <Esc><C-W>h
	inoremap <C-J> <Esc><C-W>J
	noremap <C-J> <Esc><C-W>J
	tnoremap <C-J> <Esc><C-W>J
	inoremap <C-j> <Esc><C-W>j
	noremap <C-j> <Esc><C-W>j
	tnoremap <C-j> <Esc><C-W>j
	inoremap <C-K> <Esc><C-W>K
	noremap <C-K> <Esc><C-W>K
	tnoremap <C-K> <Esc><C-W>K
	inoremap <C-k> <Esc><C-W>k
	noremap <C-k> <Esc><C-W>k
	tnoremap <C-k> <Esc><C-W>k
	inoremap <C-L> <Esc><C-W>L
	noremap <C-L> <Esc><C-W>L
	tnoremap <C-L> <Esc><C-W>L
	inoremap <C-l> <Esc><C-W>l
	noremap <C-l> <Esc><C-W>l
	tnoremap <C-l> <Esc><C-W>l
		" }}}

	" escapeisms {{{
	tnoremap <Esc> <C-\><C-n>
	tnoremap jj <C-\><C-n>
	inoremap jj <Esc>
	" }}}

	" swap : and ; {{{
	nnoremap : ;
	nnoremap ; :

	vnoremap : ;
	vnoremap ; :
	" }}}

	"Visual block far more useful than regular visual mode {{{
	nnoremap v <C-v>
	nnoremap <C-v> v
	" }}}

	" make <c-y> <c-e> more useful by doing whole words {{{
	inoremap <expr> <c-y> matchstr(getline(line('.')-1), '\%' . virtcol('.') . 'v\%(\k\+\\|.\)')
	inoremap <expr> <c-e> matchstr(getline(line('.')+1), '\%' . virtcol('.') . 'v\%(\k\+\\|.\)')
	" }}}

	" Git fugitive mappings {{{
	nnoremap <LEADER>gb :Gblame w<CR>
	nnoremap <LEADER>gd :Gdiff<SPACE>
	nnoremap <LEADER>gs :Gstatus<CR>
	nnoremap <LEADER>gc :Gcommit -v<CR>
	nnoremap <LEADER>gl :Glog<CR>
	nnoremap <LEADER>gr :Gread<CR>
	nnoremap <LEADER>ge :Gedit<CR>
	nnoremap <LEADER>gw :Gwrite<CR>
	" }}}

	" neoterm mappings {{{
	nnoremap <silent> <leader>tl :TREPLSendLine<cr>
	nnoremap <silent> <leader>tf :TREPLSendFile<cr>
	vnoremap <silent> <leader>ts :TREPLSendSelection<cr>

	nnoremap <silent> <leader>tc :call neoterm#close()<cr>
	nnoremap <silent> <c-c> :call neoterm#kill()<cr>
	" }}}

	nnoremap <Leader>er :call OpenRC()<CR>
	nnoremap <Leader>ep :call OpenFTplugin()<CR>

" }}}

" Autocmds {{{
if has("autocmd")

	augroup WHITESPACE " {{{
		autocmd!
		autocmd FileWritePre,FileAppendPre,FilterWritePre,BufWritePre * if match(expand('%'), 'fugitive://') != 0 | :call TrimWhiteSpace()
	augroup END " }}}

	augroup MAN " {{{
		autocmd!
		autocmd FileType man exe "set nolist colorcolumn=\"\" nonu"
	augroup END " }}}

	augroup reload_rc " {{{
		autocmd!
		autocmd BufWritePost $MYVIMRC source $MYVIMRC
	augroup END " }}}

endif
" }}}

" Colours {{{
	colorscheme wal

	" Highlight 81st column {{{
	highlight ColorColumn ctermbg=235
	call matchadd('ColorColumn', '\%81v', 100)
	" }}}

	" Git conflict highlight {{{
	highlight DIFFHEAD ctermfg=1
	highlight DIFFMIDDLE ctermfg=1
	highlight DIFFBRANCH ctermfg=1

	call matchadd('DIFFHEAD', '^<<<<<<< HEAD')
	call matchadd('DIFFMIDDLE', '^=======')
	call matchadd('DIFFBRANCH', '^>>>>>>>.*')
	" }}}

" }}}
