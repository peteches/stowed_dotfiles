xrandr \
   --output DP-2   --auto --primary \
   --output DP-1   --auto --above DP-2 \
   --output DP-4   --auto --left-of DP-2 --rotate right \
   --output HDMI-0 --auto --right-of DP-2
