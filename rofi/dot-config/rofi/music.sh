#!/bin/bash

music_dir="${HOME}/Music"

if [[ $# -eq 0 ]]; then
	ls ${music_dir}/*/*.pdf
else
	rifle ${@} >/dev/null
fi


