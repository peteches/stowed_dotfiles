# Documentation:
#   qute://help/configuring.html
#   qute://help/settings.html

# Uncomment this to still load settings configured via autoconfig.yml
# config.load_autoconfig()

config.set("content.proxy", "system")
config.set("tabs.position", "left")
config.set("tabs.width", "10%")
config.set("confirm_quit", ["downloads"])
config.set(
    "content.host_blocking.lists",
    [
        "https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts",
        "file:///home/peteches/.config/qutebrowser/blocked-hosts",
    ],
)
config.set("content.headers.accept_language", "en-GB, en")
config.set("content.pdfjs", True)
config.set("downloads.remove_finished", 300)
config.set("editor.command", ["urxvt", "-e", "nvim", "{file}"])
config.set(
    "fonts.monospace",
    '"InconsolataForPowerlineNerdFontCompleteMono - Medium" "xos4 Terminus", "Terminus", "Monospace", "DejaVu Sans Mono", "Monaco", "Bitstream Vera Sans Mono", "Andale Mono", "Courier New", "Courier", "Liberation Mono", "monospace", "Fixed", "Consolas", "Terminal"',
)
config.set("hints.mode", "number")
config.set("session.lazy_restore", True)
config.set("tabs.select_on_remove", "last-used")

config.set("content.javascript.enabled", True, "file://*")
config.set("content.javascript.enabled", True, "chrome://*/*")
config.set("content.javascript.enabled", True, "qute://*/*")

config.source("./qutewal.py")
