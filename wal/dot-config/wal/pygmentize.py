# -*- coding: utf-8 -*-
"""
    pygments.styles.vim
    ~~~~~~~~~~~~~~~~~~~

    A highlighting style for Pygments, inspired by vim.

    :copyright: Copyright 2006-2019 by the Pygments team, see AUTHORS.
    :license: BSD, see LICENSE for details.
"""

from pygments.style import Style
from pygments.token import (Comment, Error, Generic, Keyword, Name, Number,
                            Operator, String, Token, Whitespace)


class WalStyle(Style):
    """
    Styles using generated colours from pywal
    """

    background_color = "{background}"
    highlight_color = "{color15}"
    default_style = "{foreground}"

    styles = {{
            Token: "{color15}",
            Whitespace: "{color0}",
            Comment: "{color2}",
            Comment.Preproc: "",
            Comment.Special: "bold {color2}",
            Keyword: "{color3}",
            Keyword.Declaration: "{color11}",
            Keyword.Namespace: "{color3}",
            Keyword.Pseudo: "",
            Keyword.Type: "bold {color3}",
            Operator: "{color6}",
            Operator.Word: "{color14}",
            Name: "",
            Name.Class: "{color5}",
            Name.Builtin: "{color7}",
            Name.Exception: "bold {color1}",
            Name.Variable: "{color10}",

            String: "{color15}",
            Number: "{color14}",

            Generic.Heading: "bold {color4}",
            Generic.Subheading: "bold {color12}",
            Generic.Deleted: "{color9}",
            Generic.Inserted: "{color10}",
            Generic.Error: "bold {color1}",
            Generic.Emph: "italic",
            Generic.Strong: "bold",
            Generic.Prompt: "bold {foreground}",
            Generic.Output: "{foreground}",
            Generic.Traceback: "bold {color15}",

            Error: "border:{color1}",
        }}
