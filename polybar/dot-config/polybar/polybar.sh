#!/bin/bash

#wait for the process to shutdown
pgrep -x polybar | xargs kill


# launch polybar
for mon in $(xrandr --listactivemonitors | awk '/^ [0-9]+:/{print $NF}'); do
	polybar --quiet --config=${HOME}/.config/polybar/$(hostname -s).polybar $mon &
done
notify-send "Polybar launched..."
