#!/usr/bin/env zsh

setopt extendedglob

for dir in *~*_^($(hostname -s))(/); do
	stow --restow --target=${HOME} --dotfiles ${dir}
done
